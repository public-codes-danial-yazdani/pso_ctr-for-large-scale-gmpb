%*********************************PSO_CTR****************************************
%Author: Danial Yazdani
%Last Edited: June 11, 2021
%
% ------------
% Reference:
% ------------
%
%          D. Yazdani et al.,
%            "Scaling Up Dynamic Optimization Problems-A Divide-and-Conquer Approach,"
%            IEEE Transactions on Evolutionary Computation (2019).
% 
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail.com
%         danial.yazdani AT yahoo dot com
% Copyright notice: (c) 2021 Danial Yazdani
%**************************************************************************************************
function [Pso] = mPSO_freeSwarm(Pso)
global Benchmark;
if Benchmark{1}.RecentChange ==1 || Benchmark{1}.Terminate == 1
    return;
end
%% Sub-swarm movement
ii = Pso.FreeSwarmID;
Pso.pso(ii).Velocity = Pso.x * (Pso.pso(ii).Velocity + (Pso.c1 * rand(Pso.PopulationSize , Pso.Dimension).*(Pso.pso(ii).PbestPosition - Pso.pso(ii).X)) + (Pso.c2*rand(Pso.PopulationSize , Pso.Dimension).*(repmat(Pso.pso(ii).GbestPosition,Pso.PopulationSize,1) - Pso.pso(ii).X)));
Pso.pso(ii).X = Pso.pso(ii).X + Pso.pso(ii).Velocity;
for jj=1 : Pso.PopulationSize
    for kk=1 : Pso.Dimension
        if Pso.pso(ii).X(jj,kk) > Pso.MaxCoordinate
            Pso.pso(ii).X(jj,kk) = Pso.MaxCoordinate;
            Pso.pso(ii).Velocity(jj,kk) = 0;
        elseif Pso.pso(ii).X(jj,kk) < Pso.MinCoordinate
            Pso.pso(ii).X(jj,kk) = Pso.MinCoordinate;
            Pso.pso(ii).Velocity(jj,kk) = 0;
        end
    end
end
[Pso.pso(ii).FitnessValue] = Fitness(Pso.pso(ii).X,Pso.Variables);
if Benchmark{1}.RecentChange ==1 || Benchmark{1}.Terminate == 1
    return;
end
for jj=1 : Pso.PopulationSize
    if Pso.pso(ii).FitnessValue(jj) > Pso.pso(ii).PbestValue(jj)
        Pso.pso(ii).PbestValue(jj) = Pso.pso(ii).FitnessValue(jj);
        Pso.pso(ii).PbestPosition(jj,:) = Pso.pso(ii).X(jj,:);
    end
end
[BestPbestValue,BestPbestID] = max(Pso.pso(ii).PbestValue);
if BestPbestValue>Pso.pso(ii).GbestValue
    Pso.pso(ii).GbestValue = BestPbestValue;
    Pso.pso(ii).GbestPosition = Pso.pso(ii).PbestPosition(BestPbestID,:);
end
%% Exclusion
ii = Pso.FreeSwarmID;
for jj=1 : Pso.SwarmNumber
    if ii~=jj && pdist2(Pso.pso(ii).GbestPosition,Pso.pso(jj).GbestPosition)<Pso.ExclusionLimit
        [Pso.pso(ii)] = InitializingPSO(Pso.Dimension,Pso.PopulationSize,Pso.Variables);
        if Benchmark{1}.RecentChange ==1 || Benchmark{1}.Terminate == 1
            return;
        end
        break;
    end
end
%% FreeSwarm Convergence
CovergenceFlag = 1;
for ii=1 : Pso.PopulationSize
    for jj=1 : Pso.PopulationSize
        if CovergenceFlag==1 && ii~=jj
            if pdist2(Pso.pso(Pso.FreeSwarmID).X(ii,:),Pso.pso(Pso.FreeSwarmID).X(jj,:))>Pso.ConvergenceLimit
                CovergenceFlag = 0;
            end
        end
    end
end
if CovergenceFlag==1
    Pso.SwarmNumber = Pso.SwarmNumber+1;
    Pso.FreeSwarmID = Pso.SwarmNumber;
    [Pso.pso(Pso.FreeSwarmID)] = InitializingPSO(Pso.Dimension,Pso.PopulationSize,Pso.Variables);
    if Benchmark{1}.RecentChange ==1 || Benchmark{1}.Terminate == 1
        return;
    end
    Pso.ExclusionLimit = 0.5 * ((Pso.MaxCoordinate-Pso.MinCoordinate) / ((Pso.SwarmNumber) ^ (1 / Pso.Dimension)));
    if Pso.ExclusionLimit > Pso.MinExclusionLimit
        Pso.ExclusionLimit = Pso.MinExclusionLimit;
    end
end
end