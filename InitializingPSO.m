%*********************************PSO_CTR****************************************
%Author: Danial Yazdani
%Last Edited: June 11, 2021
%
% ------------
% Reference:
% ------------
%
%          D. Yazdani et al.,
%            "Scaling Up Dynamic Optimization Problems-A Divide-and-Conquer Approach,"
%            IEEE Transactions on Evolutionary Computation (2019).
% 
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail.com
%         danial.yazdani AT yahoo dot com
% Copyright notice: (c) 2021 Danial Yazdani
%**************************************************************************************************
function [Pso] = InitializingPSO(Dimension, PopulationSize,Variables)
global Benchmark;
if Benchmark{1}.RecentChange == 1
    return;
end
Pso.X = Benchmark{1}.LB + ((Benchmark{1}.UB-Benchmark{1}.LB).*rand(PopulationSize,Dimension));
[Pso.FitnessValue] = Fitness(Pso.X,Variables);
Pso.PbestPosition = Pso.X;
Pso.PbestValue = Pso.FitnessValue;
[Pso.GbestValue,GbestID] = max(Pso.PbestValue);
Pso.GbestPosition = Pso.PbestPosition(GbestID,:);
Pso.Gbest_past_environment = NaN(1,Dimension);
Pso.FGbest_past_environment = NaN(1,1);
Pso.Velocity = zeros(PopulationSize,Dimension);
Pso.BestFoundPosition = Pso.GbestPosition;
Pso.Shifts = [];
Pso.ShiftSeverity = 1;
Pso.Heights = [];
Pso.HeightVar = 1;
Pso.Active=1;
end