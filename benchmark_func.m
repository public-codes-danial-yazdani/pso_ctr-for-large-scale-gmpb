%*********************************Generalized Moving Peaks Benchmark (GMPB)****************************************
%Author: Danial Yazdani
%Last Edited: June 11, 2021
%
% ------------
% Reference:
% ------------
%
%  D. Yazdani et al.,
%            "Benchmarking Continuous Dynamic Optimization: Survey and Generalized Test Suite,"
%            IEEE Transactions on Cybernetics (2020).
% 
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail.com
%         danial.yazdani AT yahoo dot com
% Copyright notice: (c) 2021 Danial Yazdani
%**************************************************************************************************
function [result] = benchmark_func(X)
global Benchmark;
persistent  Optimum;
if isempty(Optimum)
    Optimum = OptimumValue();
end
[SolutionNumber,~] = size(X);
result = NaN(SolutionNumber,1);
for jj=1 : SolutionNumber
    if Benchmark{1}.RecentChange == 1
        return;
    end
x = X(jj,:)';
    fit = 0;
    ldim = 1;
    for ii=1 : Benchmark{1}.MPBnumber
        solution = x(Benchmark{1}.PermutationMap(ldim:ldim+Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.Dimension-1));
        f=zeros(Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.ComponentNumber,1);
        if ~isnan(solution)
            for k=1 : Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.ComponentNumber
                a = Transform((solution - Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.ComponentsPosition(k,:)')'*Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.RotationMatrix(:,:,k)',Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.tau(k),Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.eta(k,:));
                b = Transform(Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.RotationMatrix(:,:,k) * (solution - Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.ComponentsPosition(k,:)'),Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.tau(k),Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.eta(k,:));
                f(k) = Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.ComponentsHeight(k) - sqrt( a * diag(Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.ComponentsWidth(k,:).^2) * b);
            end
            f = max(f) * Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.Weight;
        end
        ldim = ldim + Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.Dimension;
        fit = fit + f;
    end
    result(jj) = fit;
    Benchmark{1}.FitnessEvaluationCounter = Benchmark{1}.FitnessEvaluationCounter + 1;
    if Benchmark{1}.FitnessEvaluationCounter >= Benchmark{1}.MaxEvals
        clear Optimum CurrentFE;
        Benchmark{1}.Terminate = 1;
        return;
    end
    if ~rem(Benchmark{1}.FitnessEvaluationCounter,Benchmark{1}.ChangeFrequency)
        Benchmark{1}.RecentChange = 1;
        clear Optimum CurrentFE;
        return;
    end
end
end
function Y = Transform(X,tau,eta)
Y = X;
tmp = (X > 0);
Y(tmp) = log(X(tmp));
Y(tmp) = exp(Y(tmp) + tau*(sin(eta(1).*Y(tmp)) + sin(eta(2).*Y(tmp))));
tmp = (X < 0);
Y(tmp) = log(-X(tmp));
Y(tmp) = -exp(Y(tmp) + tau*(sin(eta(3).*Y(tmp)) + sin(eta(4).*Y(tmp))));
end