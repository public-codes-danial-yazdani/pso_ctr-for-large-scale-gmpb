%*********************************Generalized Moving Peaks Benchmark (GMPB)****************************************
%Author: Danial Yazdani
%Last Edited: June 11, 2021
%
% ------------
% Reference:
% ------------
%
%  D. Yazdani et al.,
%            "Benchmarking Continuous Dynamic Optimization: Survey and Generalized Test Suite,"
%            IEEE Transactions on Cybernetics (2020).
% 
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail dot com
%         danial.yazdani AT yahoo dot com
% Copyright notice: (c) 2021 Danial Yazdani
%**************************************************************************************************
function OptVal = OptimumValue()
global Benchmark;
fit = NaN(1,Benchmark{1}.MPBnumber);
for ii=1 : Benchmark{1}.MPBnumber
    fit(ii) = max(Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.ComponentsHeight)*Benchmark{Benchmark{1}.Environmentcounter}.MPB{ii}.Weight;
end
OptVal = sum(fit);  