%*********************************Generalized Moving Peaks Benchmark (GMPB)****************************************
%Author: Danial Yazdani
%Last Edited: June 11, 2021
%
% ------------
% Reference:
% ------------
%
%  D. Yazdani et al.,
%            "Benchmarking Continuous Dynamic Optimization: Survey and Generalized Test Suite,"
%            IEEE Transactions on Cybernetics (2020).
%
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail dot com
%         danial.yazdani AT yahoo dot com
% Copyright notice: (c) 2021 Danial Yazdani
%**************************************************************************************************
function MPB = InitializingComponents(d,ii,D,Challenge)
MPB.id              = ii;
MPB.Dimension       = d;
MPB.MinCoordinate   = -50;
MPB.MaxCoordinate   = 50;
MPB.MinHeight       = 30;
MPB.MaxHeight       = 70;
MPB.MinWidth        = 1;
MPB.MaxWidth        = 12;
MPB.MinAngle        = -pi;
MPB.MaxAngle        = pi;
MPB.Weight          = (d/D) * (0.5 + rand*2.5);
MPB.HeightSeverity  = 5 + rand*4;
MPB.WidthSeverity   = 0.5 + rand*1;
MPB.AngleSeverity   = (pi/12) + rand*(pi/12);
MPB.TauSeverity     = 0.025 + rand*0.05;
MPB.EtaSeverity     = 1 + rand*2;
if Challenge.ComponentNumber==0
    MPB.ComponentNumber     = 4 + ceil(rand*11);%in{5,...,15}
else
    MPB.ComponentNumber     = 14 + ceil(rand*21);%in{15,...,35}
end
if Challenge.ShiftSeverity==0
    MPB.ShiftSeverity  = 1 + rand*2;
else
    MPB.ShiftSeverity  = 3 + rand*2;
end
MPB.MinTau        = -0.5;
MPB.MaxTau        = 0.5;
MPB.MinEta        = -20;
MPB.MaxEta        = 20;
MPB.ComponentsPosition = MPB.MinCoordinate + ((MPB.MaxCoordinate-MPB.MinCoordinate)*rand(MPB.ComponentNumber,MPB.Dimension));
MPB.ComponentsHeight   = MPB.MinHeight     + (MPB.MaxHeight-MPB.MinHeight)*rand(MPB.ComponentNumber,1);
MPB.ComponentsWidth    = MPB.MinWidth      + (MPB.MaxWidth-MPB.MinWidth)*rand(MPB.ComponentNumber,d);
MPB.ComponentsAngle    = MPB.MinAngle      + (MPB.MaxAngle-MPB.MinAngle)*rand(MPB.ComponentNumber,1);
MPB.tau           = MPB.MinTau        + (MPB.MaxTau-MPB.MinTau)*rand(MPB.ComponentNumber,1);
MPB.eta           = MPB.MinEta        + (MPB.MaxEta-MPB.MinEta)*rand(MPB.ComponentNumber,4);
MPB.InitialRotationMatrix = NaN(d,d,MPB.ComponentNumber);
for ii=1 : MPB.ComponentNumber
    [MPB.InitialRotationMatrix(:,:,ii) , ~] = qr(rand(d));
end
if d>1%there is no ill-conditioning and rotation in 1-dimensional MPBs
    MPB.RotationMatrix = MPB.InitialRotationMatrix;
else%for 1-dimensional subfunctions
    MPB.RotationMatrix = repmat(eye(d),1,1,MPB.ComponentNumber);
    MPB.AngleSeverity  = 0;
end
end