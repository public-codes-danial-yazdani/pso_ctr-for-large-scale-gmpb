%*********************************Generalized Moving Peaks Benchmark (GMPB)****************************************
%Author: Danial Yazdani
%Last Edited: June 11, 2021
%
% ------------
% Reference:
% ------------
%
%  D. Yazdani et al.,
%            "Benchmarking Continuous Dynamic Optimization: Survey and Generalized Test Suite,"
%            IEEE Transactions on Cybernetics (2020).
% 
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail dot com
%         danial.yazdani AT yahoo dot com
% Copyright notice: (c) 2021 Danial Yazdani
%**************************************************************************************************
function Benchmark = BenchmarkGenerator(ScenarioNumber,EnvironmentNumber)
Benchmark.Challenge.ShiftSeverity       = 0;%Set it to a non-zero value to activate challenging setting based on the shift severity.
Benchmark.Challenge.ComponentNumber     = 0;%Set it to a non-zero value to activate challenging setting based on the number of components.
Benchmark.Challenge.ChangeFrequency     = 0;%Set it to a non-zero value to activate challenging setting based on the change frequency.
switch  ScenarioNumber
    case 1
        Benchmark.ComponentDimensions = [2, 3, 5, 6, 6, 8 , 10];
        FullySeparableDimension = 10;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 2
        Benchmark.ComponentDimensions = [2, 3, 5 , 5];
        FullySeparableDimension = 35;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 3
        Benchmark.ComponentDimensions = [2, 2, 3, 5, 5, 5, 5, 5 , 8 , 10];
        FullySeparableDimension = 0;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 4
        Benchmark.ComponentDimensions = [];
        FullySeparableDimension = 50;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 5
        Benchmark.ComponentDimensions = [50];
        FullySeparableDimension = 0;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 6
        Benchmark.ComponentDimensions = [2 , 2 , 3 , 5 , 5 ,  6 , 6 , 8 , 8 , 10 , 10 , 15];
        FullySeparableDimension = 20;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 7
        Benchmark.ComponentDimensions = [2 , 2 , 3 , 3 , 5 , 5 , 10];
        FullySeparableDimension = 70;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 8
        Benchmark.ComponentDimensions = [2 , 2 , 2 , 2 , 3 , 3 , 5 , 5 , 5 , 5 , 5 , 5 , 8 , 8 , 10 , 10, 20];
        FullySeparableDimension = 0;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 9
        Benchmark.ComponentDimensions = [];
        FullySeparableDimension = 100;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 10
        Benchmark.ComponentDimensions = [100];
        FullySeparableDimension = 0;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 11
        Benchmark.ComponentDimensions = [2 , 2 , 3 , 5 , 5 ,  6 , 6 , 8 , 8 , 10 , 10 , 15 , 20 , 20 , 30];
        FullySeparableDimension = 50;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 12
        Benchmark.ComponentDimensions = [2 , 3 , 5 , 10 , 20 , 30];
        FullySeparableDimension = 130;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 13
        Benchmark.ComponentDimensions = [2 , 2 , 2 , 3 , 5 , 5 , 5 , 5 , 5 , 8 , 8 , 10 , 10 , 10 , 20 , 20 , 30 , 50];
        FullySeparableDimension = 0;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 14
        Benchmark.ComponentDimensions = [];
        FullySeparableDimension = 200;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    case 15
        Benchmark.ComponentDimensions = [200];
        FullySeparableDimension = 0;
        Benchmark.ComponentDimensions = [Benchmark.ComponentDimensions,ones(1,FullySeparableDimension)];
    otherwise
        warning('Invalid secnario number. You must choose between scenarios 1 to 15')
end
Benchmark.Dimension = sum(Benchmark.ComponentDimensions);
if Benchmark.Challenge.ChangeFrequency==0
    Benchmark.ChangeFrequency = 500*Benchmark.Dimension;
else
    Benchmark.ChangeFrequency = 200*Benchmark.Dimension;
end
Benchmark.PermutationMap = randperm(Benchmark.Dimension);
Benchmark.UB = 50;
Benchmark.LB = -50;
Benchmark.MPBnumber = length(Benchmark.ComponentDimensions);
Benchmark.MPB = cell(1,Benchmark.MPBnumber);
for ii=1 : Benchmark.MPBnumber
    Benchmark.MPB{ii} = InitializingComponents(Benchmark.ComponentDimensions(ii),ii,Benchmark.Dimension,Benchmark.Challenge);
end
Benchmark.EnvironmentNumber        = EnvironmentNumber;
Benchmark.Environmentcounter       = 1;
Benchmark.FitnessEvaluationCounter = 0;
Benchmark.MaxEvals                 = Benchmark.ChangeFrequency * Benchmark.EnvironmentNumber;
Benchmark.RecentChange             = 0;
Benchmark.Terminate                = 0;
end