%*********************************PSO_CTR****************************************
%Author: Danial Yazdani
%Last Edited: June 11, 2021
%
% ------------
% Reference:
% ------------
%
%          D. Yazdani et al.,
%            "Scaling Up Dynamic Optimization Problems-A Divide-and-Conquer Approach,"
%            IEEE Transactions on Evolutionary Computation (2019).
% 
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail.com
%         danial.yazdani AT yahoo dot com
% Copyright notice: (c) 2021 Danial Yazdani
%**************************************************************************************************
function [PSO] = Reaction(PSO)
for ii=1 : length(PSO)
    %% Updating Shift Severity
    for jj=1 : PSO{1, ii}.SwarmNumber
        if jj~=PSO{1, ii}.FreeSwarmID
            if sum(isnan(PSO{1, ii}.pso(jj).Gbest_past_environment))==0
                PSO{1, ii}.pso(jj).Shifts = [PSO{1, ii}.pso(jj).Shifts , pdist2(PSO{1, ii}.pso(jj).Gbest_past_environment,PSO{1, ii}.pso(jj).GbestPosition)];
                PSO{1, ii}.pso(jj).ShiftSeverity = mean(PSO{1, ii}.pso(jj).Shifts);
            else
                PSO{1, ii}.pso(jj).ShiftSeverity = 1;
            end
        end
    end
    %% Introducing diversity (all except free swarm)
    for jj=1 : PSO{1, ii}.SwarmNumber
        if jj~=PSO{1, ii}.FreeSwarmID
            PSO{1, ii}.pso(jj).X = repmat(PSO{1, ii}.pso(jj).GbestPosition,PSO{1, ii}.PopulationSize,1)+ (rands(PSO{1, ii}.PopulationSize,PSO{1, ii}.Dimension)*PSO{1, ii}.pso(jj).ShiftSeverity);
            PSO{1, ii}.pso(jj).X(1,:) = PSO{1, ii}.pso(jj).GbestPosition;
        end
    end
    %% Updating memory
    for jj=1 : PSO{1, ii}.SwarmNumber
        [PSO{1, ii}.pso(jj).FitnessValue] = Fitness(PSO{1, ii}.pso(jj).X,PSO{1, ii}.Variables);
        PSO{1, ii}.pso(jj).PbestValue = PSO{1, ii}.pso(jj).FitnessValue;
        PSO{1, ii}.pso(jj).PbestPosition = PSO{1, ii}.pso(jj).X;
        PSO{1, ii}.pso(jj).Gbest_past_environment = PSO{1, ii}.pso(jj).GbestPosition;
        PSO{1, ii}.pso(jj).FGbest_past_environment = PSO{1, ii}.pso(jj).GbestValue;
        [PSO{1, ii}.pso(jj).GbestValue,BestPbestID] = max(PSO{1, ii}.pso(jj).PbestValue);
        PSO{1, ii}.pso(jj).GbestPosition = PSO{1, ii}.pso(jj).PbestPosition(BestPbestID,:);
    end
    %% Activation
    for jj=1 : PSO{1, ii}.SwarmNumber
        PSO{1, ii}.pso(jj).Active = 1;
    end
end