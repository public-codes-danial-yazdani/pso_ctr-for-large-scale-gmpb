%*********************************DG2***********************************************************
%Author: Danial Yazdani
%Last Edited: June 13, 2017
%
% ------------
% Reference:
% ------------
%
%     M. N. Omidvar et al.,
%           "A Faster and More Accurate Differential Grouping for Large-Scale Black-Box Optimization. 
%           IEEE Transactions on Evolutionary Computation (2017)
% 
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Mohammad Nabi Omidvar
% e-mail: mn.omidvar AT gmail.com
% Copyright notice: (c) 2017 Mohammad Nabi Omidvar
%**************************************************************************************************
function [components] = findConnComp(C)

L=size(C,1); % number of vertex

% Breadth-first search:
labels=zeros(1,L); % all vertex unexplored at the begining
rts=[];
ccc=0; % connected components counter
while true
    ind=find(labels==0);
    if ~isempty(ind)
        fue=ind(1); % first unexplored vertex
        rts=[rts fue];
        list=[fue];
        ccc=ccc+1;
        labels(fue)=ccc;
        while true
            list_new=[];
            for lc=1:length(list)
                p=list(lc); % point
                cp=find(C(p,:)); % points connected to p
                cp1=cp(labels(cp)==0); % get only unexplored vertecies
                labels(cp1)=ccc;
                list_new=[list_new cp1];
            end
            list=list_new;
            if isempty(list)
                break;
            end
        end
    else
        break;
    end
end

group_num = max(labels);
allgroups = cell(1, group_num);
for i = 1:group_num
    allgroups{i} = find(labels == i); 
end

components = allgroups;
